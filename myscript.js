function displayUserList(){
    $.ajax({
        url: 'http://localhost:3000/users',
        type: 'GET',
        
        error: function(XMLHttpRequest, textStatus, errorThrown){
            console.log('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
        },
        success: function(res){
        $('#showUsers').empty();

            res.forEach(function(user){
                console.log(user);
                $('#showUsers').append(user.first_name + " " + user.last_name + " ");
                $('#showUsers').append('<a href="http://localhost:3000/users/' + user.user_id + '">'+user.first_name + " " + user.last_name+'</a>');
                $('#showUsers').append('</br>');
            });
        }
    });
};

$('#registration').submit(function(e){
    e.preventDefault(); // avoid to execute the actual submit of the form

    let form = $(this);
    let url = form.attr('action');

    $.ajax({
        type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(response){
                displayUserList();
                console.log (user);
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            }, 
    });
    return false;
});

function showUsersInfo(){
    const regex = /^\d{1,2}/gm;
    let user_a = document.getElementById('user_a').value;
    if (!regex.test(user_a)){
        alert ('Please Enter a number');
        return false;
    } else 
    $.ajax({
        url: 'http://localhost:3000/users/' + user_a,
        type: 'GET',
        
        error: function(XMLHttpRequest, textStatus, errorThrown){
            console.log('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
        },
        success: function(res){
        $('#showUsers').empty();
            if (res.first_name !== undefined) {
            $('#showUsers').append(res.first_name + ' ' + res.last_name + ' ' + res.email + " " + res.birthday +  '</br>');
            } else {
                $('#showUsers').empty()
                $('#showUsers').append('This user is not registered yet.');
            }   
        }
    });
};