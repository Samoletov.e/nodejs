const sqlite3 = require('sqlite3');
const db = new sqlite3.Database('olympia.db');

// run each database statement *serially* one after another
// (if you don't do this, then all statements will run in parallel,
//  which we don't want)
db.serialize(() => {
  // create a new database table:
  db.run("CREATE TABLE tblOlympia (user_id INTEGER PRIMARY KEY, first_name TEXT, last_name TEXT, email TEXT, birthday DATE)");

  // insert 3 rows of data:
  db.run("INSERT INTO tblOlympia (first_name, last_name, email, birthday) VALUES ('Ronald','Coleman', 'RonaldColeman@gmail.com', '13.05.1964')");
  db.run("INSERT INTO tblOlympia (first_name, last_name, email, birthday) VALUES ('Lee', 'Haney', 'LeeHaney@gmail.com', '11.11.1959')");
  db.run("INSERT INTO tblOlympia (first_name, last_name, email, birthday) VALUES ('Arnold', 'Schwarzenegger', 'ArnoldSchwarzenegger@gmail.com', '30.07.1947')");

  console.log('successfully created the tblTeam table in olympia.db');

  // print them out to confirm their contents:
  db.each("SELECT user_id, first_name, last_name, email, birthday FROM tblOlympia", (err, row) => {
      console.log(row.user_id + " " + row.first_name + " " + row.last_name + " " + row.email + " " + row.birthday);
  });
});

db.close();
