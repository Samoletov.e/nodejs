const express = require('express');
const sqlite3 = require('sqlite3');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

const db = new sqlite3.Database('olympia.db');

app.use(express.static(__dirname));

app.use(bodyParser.urlencoded({extended: true})); // hook up with your app

app.get('/',(req, res) => {
        res.send('We are on the server');
});

app.get('/users',(req, res) => {
    db.all('SELECT user_id, first_name, last_name, email, birthday FROM tblOlympia', (err, rows) => {
        res.send(rows);
    });
});

app.get('/users/:userid', (req, res) => {
    const idToLookup = req.params.userid; // matches ':userid' above
    console.log(idToLookup);
    // db.all() fetches all results from an SQL query into the 'rows' variable:
    db.all(
      'SELECT * FROM tblOlympia WHERE user_id=$user_id',
      // parameters to SQL query:
      {
        $user_id: idToLookup
      },
      // callback function to run when the query finishes:
      (err, rows) => {
        //console.log(rows);
        if (rows.length > 0) {
          res.send(rows[0]);
        } else {
          res.send({}); // failed, so return an empty object instead of undefined
        }
      }
    );
  });

  app.post('/users', (req, res) => {
    console.log(req.body);
    
  let sql = 'INSERT INTO tblOlympia (first_name, last_name, email, birthday) VALUES ($first_name, $last_name, $email, $birthday)'
  db.run(
      sql,
      // parameters to SQL query:
      {
        $first_name: req.body.first_name,
        $last_name: req.body.last_name,
        $email: req.body.email,
        $birthday: req.body.birthday,
      },
      // callback function to run when the query finishes:
      (err) => {
        if (err) {
          res.send({message: 'error in app.post(/users)'});
        } else {
          res.send({message: 'successfully run app.post(/users)'});
        }
      });
  });

app.listen(port, () => {
    console.log('Server started at http://localhost:3000/');
});